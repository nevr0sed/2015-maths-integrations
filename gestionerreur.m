function gerreur = gestionerreur(fun, result, xmin, xmax, N)
	gestionerreur = abs((quad(fun,xmin,xmax,N) - result) / (quad(fun, xmin,xmax, N))) * 100;
		gestionerreur
endfunction