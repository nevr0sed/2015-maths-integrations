function mathpiva(fun, xmin, xmax, N, meth)
	
	if (!exist("fun", "var") || !exist("xmin", "var") || !exist("xmax", "var") || !exist("N", "var") || !exist("meth", "var") )
		printf("USAGE mathpiva fonction, xmin, xmax, N de iterations, methode int\n ");
		printf("methodes d'ingegration : 1 = de Droite, 2 Gauche, 3 Milieu, 4 Trapeze, 5 Simpson, 0 all.");
	else
		switch (meth)
			case 1
				% call method droite
				printf("Methode de Droite.\n");
				droite = methdroite(fun, xmin, xmax, N);
				
				printf("Gestion des erreurs de methode de droite.\n");
				gestionerreur(fun, droite, xmin, xmax, N)
			case 2
				% call method gauche
				printf("Methode de Gauche.\n");
				gauche = methgauche(fun, xmin, xmax, N);
				
				printf("Gestion des erreurs de methode de gauche.\n");
				gestionerreur(fun, gauche, xmin, xmax, N)
			case 3
				% call method point du milieu
				printf("Methode du Mileu.\n");
				milieu = methmilieu(fun, xmin, xmax, N);
				
				printf("Gestion des erreurs de methode du milieu.\n");
				gestionerreur(fun, milieu, xmin, xmax, N)
			case 4
				% call method trapeze
				printf("Methode du Trapeze.\n");
				trapeze = methtrapeze(fun, xmin, xmax, N);
				printf("Gestion des erreurs de methode du trapeze.\n");
				gestionerreur(fun, trapeze, xmin, xmax, N);

			case 5
				% call method Simpson
				printf("Methode de Simpson.\n");
				simpson = methsimpson(fun, xmin, xmax, N);
				printf("Gestion des erreurs de methode de simpson.\n");
				gestionerreur(fun, simpson, xmin, xmax, N)
			otherwise
				% default method = all
				printf("On va executer toute les methodes.\n");
				droite = methdroite(fun, xmin, xmax, N);
				gauche = methgauche(fun, xmin, xmax, N);
				trapeze = methtrapeze(fun, xmin, xmax, N);
				simpson = methsimpson(fun, xmin, xmax, N);

				printf("droite\n");
				
				printf("erreures droite\n");
				gestionerreur(fun, droite, xmin, xmax, N)

				printf("gauche\n");
				gauche
				printf("erreures gauche\n");
				gestionerreur(fun, gauche, xmin, xmax, N)

				printf("trapeze\n");
				trapeze
				printf("erreures trapeze\n");
				gestionerreur(fun, trapeze, xmin, xmax, N)

				printf("simpson\n");
				simpson
				printf("erreures simpson\n");
				gestionerreur(fun, simpson, xmin, xmax, N)	
			endswitch
		endif
endfunction