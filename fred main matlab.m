fred main matlab

function integration(Function, xmin, xmax, iteration, methode)
  if(! exist("Function", "var") || !exist("xmin", "var") || !exist("xmax", "var") || !exist("iteration", "var") || !exist("methode", "var"))
    usage();
  else
    switch (methode)
      case "-l"
        printf(methode);

      case "-r"
        printf(methode);

      case "-m"
        printf(methode);

      case "-t"
        printf(methode);

      case "-s"
        printf(methode);

      case "-a"
        printf(methode);

      otherwise
        usage();
    endswitch
  endif
endfunction