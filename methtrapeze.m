function trapeze = methtrapeze(fun, xmin, xmax, N)

t = (xmax-xmin) / N;
summ = 0.0;
tmp_x = xmin;
	while(tmp_x < xmax ) 
		summ += fun(tmp_x) * t + (fun(tmp_x + t)-fun(tmp_x))*t/2;
		tmp_x += t;
	endwhile
	trapeze = summ;
	return;

endfunction