function gauche = methgauche(fun, xmin, xmax, N)
 
t = (xmax-xmin) / N;
summ = 0.0;
tmp_x = xmin;
	while(tmp_x < xmax ) 
		summ += fun(tmp_x) * t;
		tmp_x += t;
	endwhile
	
	gauche = summ;
	return;

endfunction