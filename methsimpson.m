function simpson = methsimpson(fun, xmin, xmax, N)

t = (xmax-xmin) / N;
summ = 0.0;
summ1 = fun(xmin + t / 2);
summ2 = 0.0;
	for i = 1:(N-1)
		summ1 += fun(xmin + t * i + t / 2);
		summ2 += fun(xmin + t * i);
	endfor
	
	summ = (t / 6) * (fun(xmin) + fun(xmax) + 4 * summ1 + 2 * summ2);
	simpson = summ;
	return;

endfunction